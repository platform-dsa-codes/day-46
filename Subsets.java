import java.util.*;

class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        // Start with an empty subset
        List<Integer> subset = new ArrayList<>();
        generateSubsets(nums, 0, subset, result);
        return result;
    }

    private void generateSubsets(int[] nums, int index, List<Integer> subset, List<List<Integer>> result) {
        // Add the current subset to the result
        result.add(new ArrayList<>(subset));

        // Iterate through the array starting from 'index'
        for (int i = index; i < nums.length; i++) {
            // Include the current element in the subset
            subset.add(nums[i]);
            // Recursively generate subsets starting from the next index
            generateSubsets(nums, i + 1, subset, result);
            // Exclude the current element to backtrack
            subset.remove(subset.size() - 1);
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array:");
        int n = scanner.nextInt();
        int[] nums = new int[n];
        System.out.println("Enter the elements:");
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }
        Solution solution = new Solution();
        List<List<Integer>> subsets = solution.subsets(nums);
        System.out.println("The subsets are:");
        for (List<Integer> subset : subsets) {
            System.out.println(subset);
        }
    }
}
