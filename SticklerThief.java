//{ Driver Code Starts
import java.util.*;
import java.io.*;

class GFG
 {
	public static void main (String[] args)
	 {
	  
	  //taking input using Scanner class
	  Scanner sc = new Scanner(System.in);
	  
	  //taking count of testcases
	  int t = sc.nextInt();
	  while(t-- > 0){
	      
	      //taking count of houses
	      int n = sc.nextInt();
	      int arr[] = new int[n];
	      
	      //inserting money present in 
	      //each house in the array
	      for(int i = 0; i<n; ++i)
	           arr[i] = sc.nextInt();
  	      
  	      //calling method FindMaxSum() of class solve
  	      System.out.println(new Solution().FindMaxSum(arr, n));
	 }
   }
}
// } Driver Code Ends


class Solution {
    // Function to find the maximum money the thief can get.
    public int FindMaxSum(int arr[], int n) {
        if (n == 0) // If there are no houses, return 0.
            return 0;
        if (n == 1) // If there is only one house, return its money.
            return arr[0];
        
        int incl = arr[0]; // Maximum money including the first house.
        int excl = 0; // Maximum money excluding the first house.
        
        for (int i = 1; i < n; i++) {
            // Current maximum money excluding the current house is maximum
            // of previous incl and excl.
            int newExcl = Math.max(incl, excl);
            
            // Current maximum money including the current house is the
            // sum of money in the current house and previous excl.
            incl = excl + arr[i];
            
            // Update excl for the next iteration.
            excl = newExcl;
        }
        
        // Return the maximum of incl and excl, which represents the maximum
        // money that the thief can get.
        return Math.max(incl, excl);
    }
}
